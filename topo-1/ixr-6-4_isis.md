### IXR-6-4: Basic ISIS configuration
```
# info from running network-instance default protocols isis
    network-instance default {
        protocols {
            isis {
                instance default_v6 {
                    admin-state enable
                    level-capability L2
                    max-ecmp-paths 64
                    net [
                        49.0001.1920.0000.2002.00
                    ]
                    ipv4-unicast {
                        admin-state enable
                    }
                    ipv6-unicast {
                        admin-state enable
                    }
                    interface ethernet-1/1.0 {
                        passive true
                    }
                    interface ethernet-2/1.0 {
                        circuit-type point-to-point
                    }
                    interface ethernet-2/2.0 {
                        circuit-type point-to-point
                    }
                    interface ethernet-2/3.0 {
                        circuit-type point-to-point
                    }
                    interface ethernet-2/4.0 {
                        circuit-type point-to-point
                    }
                    interface lo0.0 {
                        passive true
                    }
                }
            }
        }
    }
```

### Verification

