### Static route 0.0.0.0/0 (aka default route) pointing indirectly to a remote loopback address 192.0.2.2
```
# info from running network-instance default static-routes
    network-instance default {
        static-routes {
            route 0.0.0.0/0 {
                admin-state enable
                next-hop-group ixr-6-4
            }
        }
    }

# info from running network-instance default next-hop-groups
    network-instance default {
        next-hop-groups {
            group ixr-6-4 {
                admin-state enable
                nexthop 1 {
                    ip-address 192.0.2.2
                    admin-state enable
                }
            }
        }
    }
```
