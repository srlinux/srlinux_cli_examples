### IXR-6-3: Basic BGP configuration

```
# info from running network-instance default protocols bgp
    network-instance default {
        protocols {
            bgp {
                admin-state enable
                autonomous-system 64500
                router-id 192.0.2.1
                group iBGP {
                    admin-state enable
                    export-policy export-bgp-v4
                    next-hop-self true
                    peer-as 64500
                    ipv4-unicast {
                        advertise-ipv6-next-hops true
                        receive-ipv6-next-hops true
                    }
                }
                ipv4-unicast {
                    multipath {
                        max-paths-level-1 64
                        max-paths-level-2 64
                    }
                }
                neighbor 192.0.2.2 {
                    peer-group iBGP
                }
                neighbor 2000::192:0:2:2 {
                    peer-group iBGP
                }
            }
        }
    }
```

### Verification

```
A:IXR-6-3# show / network-instance default protocols bgp neighbor
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BGP neighbor summary for network-instance "default"
Flags: S static, D dynamic, L discovered by LLDP, B BFD enabled, - disabled, * slow
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
+----------------------+---------------------------------+----------------------+--------+------------+------------------+------------------+----------------+---------------------------------+
|       Net-Inst       |              Peer               |        Group         | Flags  |  Peer-AS   |      State       |      Uptime      |    AFI/SAFI    |         [Rx/Active/Tx]          |
+======================+=================================+======================+========+============+==================+==================+================+=================================+
| default              | 192.0.2.2                       | iBGP                 | S      | 64500      | active           | -                |                |                                 |
| default              | 2000::192:0:2:2                 | iBGP                 | S      | 64500      | established      | 7d:4h:40m:18s    | ipv4-unicast   | [1/1/1]                         |
+----------------------+---------------------------------+----------------------+--------+------------+------------------+------------------+----------------+---------------------------------+
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Summary:
2 configured neighbors, 1 configured sessions are established,0 disabled peers
0 dynamic peers
```
