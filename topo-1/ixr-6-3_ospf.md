### IXR-6-3: Basic OSPF configuration
```
# info from running network-instance default protocols ospf
    network-instance default {
        protocols {
            ospf {
                instance default_v4 {
                    admin-state enable
                    version OSPF_V2
                    router-id 192.0.2.1
                    max-ecmp-paths 64
                    asbr {
                    }
                    area 0.0.0.0 {
                        interface ethernet-1/1.0 {
                            passive true
                        }
                        interface ethernet-2/1.0 {
                            interface-type point-to-point
                        }
                        interface ethernet-2/2.0 {
                            interface-type point-to-point
                        }
                        interface ethernet-2/3.0 {
                            interface-type point-to-point
                        }
                        interface ethernet-2/4.0 {
                            interface-type point-to-point
                        }
                        interface lo0.0 {
                            passive true
                        }
                    }
                }
            }
        }
    }

```

### Verification

```
# show / network-instance default protocols ospf neighbor
=======================================================================================================
Net-Inst default OSPFv2 Instance default_v4 Neighbors
=======================================================================================================
+---------------------------------------------------------------------+
| Interface-Name    Rtr Id     State   Pri   RetxQ   Time Before Dead |
+=====================================================================+
| ethernet-2/1.0   192.0.2.2   FULL    1     0       36               |
| ethernet-2/2.0   192.0.2.2   FULL    1     0       36               |
| ethernet-2/3.0   192.0.2.2   FULL    1     0       36               |
| ethernet-2/4.0   192.0.2.2   FULL    1     0       36               |
+---------------------------------------------------------------------+
-------------------------------------------------------------------------------------------------------
No. of Neighbors: 4
```
