### IXR-6-4: Basic OSPF configuration

```
# info from running / network-instance default protocols ospf
    instance default_v4 {
        admin-state enable
        version OSPF_V2
        router-id 192.0.2.2
        max-ecmp-paths 64
        area 0.0.0.0 {
            interface ethernet-1/1.0 {
                passive true
            }
            interface ethernet-2/1.0 {
                interface-type point-to-point
            }
            interface ethernet-2/2.0 {
                interface-type point-to-point
            }
            interface ethernet-2/3.0 {
                interface-type point-to-point
            }
            interface ethernet-2/4.0 {
                interface-type point-to-point
            }
            interface lo0.0 {
                passive true
            }
        }
    }
```

### Verification
TBC
