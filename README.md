# SRLinux_CLI_examples

## Topology 1
- Connections and IP scheme

    ```mermaid
    graph LR;
    Tester_1(Tester_1)---|e-1/1\nVLAN|Dut_A(IXR-6-3);
    Dut_A---|e-2/1\n192.168.12.0/30\n2001::192:168:12:0/126|Dut_B(IXR-6-4);
    Dut_A---|e-2/2\n192.168.12.4/30\n2001::192:168:12:4/126|Dut_B;
    Dut_A---|e-2/3\n192.168.12.8/30\n2001::192:168:12:8/126|Dut_B;
    Dut_A---|e-2/4\n192.168.12.12/30\n2001::192:168:12:c/126|Dut_B;
    Dut_B---|e-1/1\nVLAN|Tester_2(Tester_2);
    ```

### Topology-1 configuration examples:
- IXR-6-3
    - [IXR-6-3: ISIS](topo-1/ixr-6-3_isis.md)
    - [IXR-6-3: OSPF](topo-1/ixr-6-3_ospf.md)
    - [IXR-6-3: BGP](topo-1/ixr-6-3_bgp.md)
- IXR-6-4
    - [IXR-6-4: ISIS](topo-1/ixr-6-4_isis.md)
    - [IXR-6-4: OSPF](topo-1/ixr-6-4_ospf.md)
    - [IXR-6-4: BGP](topo-1/ixr-6-4_bgp.md)

## Topology 2
- Connections and IP scheme

    ```mermaid
    graph LR;
    Tester_1(Tester_1)---|e-1/1\nVLAN|Dut_A(IXR-6-3);
    Dut_A---|e-2/1\n192.168.12.0/30\n2001::192:168:12:0/126|Dut_B(IXR-6-4);
    Dut_A---|e-2/2\n192.168.12.4/30\n2001::192:168:12:4/126|Dut_B;
    Dut_A---|e-2/3|Lag-1;
    Dut_A---|e-2/4|Lag-1;
    Lag-1---|e-2/3|Dut_B;
    Lag-1---|e-2/4|Dut_B;
    Dut_B---|e-1/1\nVLAN|Tester_2(Tester_2);
    ```

### Topology-2 configuration examples:
- [IXR-6-3: LAG](topo-2/ixr-6-3_LAG.md)
